package com.natoboram.spigot.firstlight;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitTask;

public class Shutdown implements Listener {
	private final FirstLight plugin;
	private BukkitTask shutdownTask = null;

	public Shutdown(FirstLight plugin) {
		this.plugin = plugin;
	}

	public void startShutdownTask() {
		FileConfiguration config = plugin.getConfig();
		int delay = config.getInt("shutdown.delay");

		// Start the shutdown task
		if (Bukkit.getOnlinePlayers().size() <= 1) {
			shutdownTask = Bukkit.getScheduler().runTaskLater(plugin, () -> {
				Bukkit.shutdown();
			}, delay);
		}
	}

	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event) {
		// Cancel the shutdown task if it's scheduled
		if (shutdownTask != null) {
			shutdownTask.cancel();
			shutdownTask = null; // Reset the shutdown task
		}

		FileConfiguration config = plugin.getConfig();
		boolean doTimeSetEnabled = config.getBoolean("startup.dotimeset.enabled");
		int timeSet = config.getInt("startup.timeset");

		// Set Time 0
		if (Bukkit.getOnlinePlayers().size() <= 1 && doTimeSetEnabled) {
			event.getPlayer().getWorld().setTime(timeSet);
		}
	}

	@EventHandler
	public void onPlayerQuit(final PlayerQuitEvent event) {
		startShutdownTask();
	}
}
